demosploit
==========

This repository contains information and some proof-of-concept code relating
to two RCE vulnerabilities in [LunaLUA], an extension module for [SMBX].

* demosploit - Buffer overflow when reading level names in the demos.dmo file
* autosploit - Limited buffer overflow when parsing autocode (commands)

The repository is named "demosploit" as it was the first vuln discovered.

[LunaLUA]:https://github.com/Wohlhabend-Networks/LunaDLL
[SMBX]:http://www.supermariobrosx.org/
